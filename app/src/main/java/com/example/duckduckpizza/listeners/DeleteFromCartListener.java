package com.example.duckduckpizza.listeners;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.duckduckpizza.NetworkService;
import com.example.duckduckpizza.activity.CartActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DeleteFromCartListener implements AdapterView.OnItemLongClickListener {

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        requestToServer((AppCompatActivity) view.getContext(), id);
        view.getContext().startActivity(new Intent(view.getContext(), CartActivity.class));
        return true;
    }

    private void requestToServer(AppCompatActivity activity, Long cartItemId) {
        Call<Void> call = NetworkService.getInstance().getJSONApi().deleteCartItem(NetworkService.authToken, cartItemId);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
