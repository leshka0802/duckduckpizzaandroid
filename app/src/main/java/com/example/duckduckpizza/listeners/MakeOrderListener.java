package com.example.duckduckpizza.listeners;

import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.duckduckpizza.NetworkService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MakeOrderListener implements View.OnClickListener {

    @Override
    public void onClick(View v) {
       AppCompatActivity activity = (AppCompatActivity) v.getContext();
       requestToServer(activity);

    }

    private void requestToServer(AppCompatActivity activity) {
        Call<Void> call = NetworkService.getInstance().getJSONApi().makeOrder(NetworkService.authToken);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 201) {
                    Toast.makeText(activity, "Order successfully created", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(activity, String.format("Something went wrong %s", response.errorBody().byteStream()), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
