package com.example.duckduckpizza.listeners;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.duckduckpizza.NetworkService;
import com.example.duckduckpizza.R;
import com.example.duckduckpizza.dto.CartItemRequestDTO;
import com.example.duckduckpizza.dto.CartItemResponseDTO;
import com.example.duckduckpizza.dto.PizzaResponseDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddToCartListener implements View.OnClickListener {

    @Override
    public void onClick(View v) {
        AppCompatActivity activity = (AppCompatActivity) v.getContext();
        if (activity != null) {
            EditText amount = activity.findViewById(R.id.amount);
            EditText pizzaId = activity.findViewById(R.id.pizzaId);
            RadioGroup radioGroup = activity.findViewById(R.id.radioGroup);
            int selectedRadioId = radioGroup.getCheckedRadioButtonId();
            if (amount.getText() != null && selectedRadioId != -1) {
                CartItemRequestDTO body = new CartItemRequestDTO();
                body.setPizza(Integer.parseInt(pizzaId.getText().toString()));
                body.setAmount(Integer.parseInt(amount.getText().toString()));
                RadioButton radioButton = activity.findViewById(selectedRadioId);
                switch (radioButton.getText().toString()) {
                    case "Small":
                        body.setSize(1);
                        break;
                    case "Middle":
                        body.setSize(2);
                        break;
                    case "Large":
                        body.setSize(3);
                        break;
                }
                requestToServer(activity, body);
            } else {
                Toast.makeText(activity, "Fill all fields", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void requestToServer(AppCompatActivity activity, CartItemRequestDTO body) {
        Call<CartItemResponseDTO> call = NetworkService.getInstance().getJSONApi().createCartItem(NetworkService.authToken, body);
        call.enqueue(new Callback<CartItemResponseDTO>() {
            @Override
            public void onResponse(Call<CartItemResponseDTO> call, Response<CartItemResponseDTO> response) {
                if (response.code() == 201) {
                    Toast.makeText(activity, "Item successfully added", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(activity, String.format("Something went wrong %s", response.errorBody().byteStream()), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CartItemResponseDTO> call, Throwable t) {
                Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
