package com.example.duckduckpizza.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.duckduckpizza.NetworkService;
import com.example.duckduckpizza.R;
import com.example.duckduckpizza.adapter.OrderAdapter;
import com.example.duckduckpizza.dto.OrderResponseDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_list);
        ListView listView = findViewById(R.id.order_item);
        Call<List<OrderResponseDTO>> call = NetworkService.getInstance().getJSONApi().getOrderData(NetworkService.authToken);
        Context context = this;
        call.enqueue(new Callback<List<OrderResponseDTO>>() {
            @Override
            public void onResponse(Call<List<OrderResponseDTO>> call, Response<List<OrderResponseDTO>> response) {
                System.out.println(response.body());
                OrderAdapter orderAdapter = new OrderAdapter(context, response.body());
                listView.setAdapter(orderAdapter);
            }

            @Override
            public void onFailure(Call<List<OrderResponseDTO>> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_pizza:
                startActivity(new Intent(this, PizzaListActivity.class));
                return true;
            case R.id.action_cart:
                startActivity(new Intent(this, CartActivity.class));
                return true;
            case R.id.action_order:
                startActivity(new Intent(this, OrderListActivity.class));
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }
}
