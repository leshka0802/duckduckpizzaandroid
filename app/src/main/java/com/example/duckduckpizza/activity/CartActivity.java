package com.example.duckduckpizza.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.duckduckpizza.NetworkService;
import com.example.duckduckpizza.R;
import com.example.duckduckpizza.adapter.CartAdapter;
import com.example.duckduckpizza.dto.CartItemResponseDTO;
import com.example.duckduckpizza.listeners.DeleteFromCartListener;
import com.example.duckduckpizza.listeners.MakeOrderListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_list);
        ListView listView = findViewById(R.id.cart_item);
        Call<List<CartItemResponseDTO>> call = NetworkService.getInstance().getJSONApi().getCartItemData(NetworkService.authToken);
        Context context = this;
        call.enqueue(new Callback<List<CartItemResponseDTO>>() {
            @Override
            public void onResponse(Call<List<CartItemResponseDTO>> call, Response<List<CartItemResponseDTO>> response) {
                System.out.println(response.body());
                CartAdapter cartAdapter = new CartAdapter(context, response.body());
                listView.setAdapter(cartAdapter);
            }

            @Override
            public void onFailure(Call<List<CartItemResponseDTO>> call, Throwable t) {

            }
        });
        listView.setLongClickable(true);
        listView.setOnItemLongClickListener(new DeleteFromCartListener());
        Button btnToCart = findViewById(R.id.makeOrder);
        btnToCart.setOnClickListener(new MakeOrderListener());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_pizza:
                startActivity(new Intent(this, PizzaListActivity.class));
                return true;
            case R.id.action_cart:
                startActivity(new Intent(this, CartActivity.class));
                return true;
            case R.id.action_order:
                startActivity(new Intent(this, OrderListActivity.class));
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

}
