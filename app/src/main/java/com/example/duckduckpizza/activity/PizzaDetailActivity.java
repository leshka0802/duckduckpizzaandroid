package com.example.duckduckpizza.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.example.duckduckpizza.NetworkService;
import com.example.duckduckpizza.R;
import com.example.duckduckpizza.dto.PizzaResponseDTO;
import com.example.duckduckpizza.listeners.AddToCartListener;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PizzaDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pizza_detail);
        Intent intent = getIntent();
        Long pizzaId = (Long) intent.getSerializableExtra("pizzaId");
        Call<PizzaResponseDTO> call = NetworkService.getInstance().getJSONApi().getPizzaDetail(pizzaId);
        call.enqueue(new Callback<PizzaResponseDTO>() {
            @Override
            public void onResponse(Call<PizzaResponseDTO> call, Response<PizzaResponseDTO> response) {
                System.out.println(response.body());
                fillDetailView(response.body());
            }

            @Override
            public void onFailure(Call<PizzaResponseDTO> call, Throwable t) {

            }
        });
        Button btnToCart = findViewById(R.id.btnToCart);
        btnToCart.setOnClickListener(new AddToCartListener());
    }

    protected void fillDetailView(PizzaResponseDTO pizzaDTO) {
        TextView pizzaId = findViewById(R.id.pizzaId);
        TextView name = findViewById(R.id.name);
        TextView description = findViewById(R.id.description);
        TextView priceAPiece = findViewById(R.id.priceAPiece);
        TextView priceSmall = findViewById(R.id.priceSmall);
        TextView priceMiddle = findViewById(R.id.priceMiddle);
        TextView priceLarge = findViewById(R.id.priceLarge);

        pizzaId.setText(Integer.toString(pizzaDTO.getId()));
        name.setText(pizzaDTO.getName());
        description.setText(pizzaDTO.getDescription());
        priceAPiece.setText(pizzaDTO.getPriceAPiece());
        priceSmall.setText(pizzaDTO.getPriceSmall());
        priceMiddle.setText(pizzaDTO.getPriceMiddle());
        priceLarge.setText(pizzaDTO.getPriceLarge());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_pizza:
                startActivity(new Intent(this, PizzaListActivity.class));
                return true;
            case R.id.action_cart:
                startActivity(new Intent(this, CartActivity.class));
                return true;
            case R.id.action_order:
                startActivity(new Intent(this, OrderListActivity.class));
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }
}
