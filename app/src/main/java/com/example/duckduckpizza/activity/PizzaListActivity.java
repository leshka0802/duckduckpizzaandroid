package com.example.duckduckpizza.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.duckduckpizza.NetworkService;
import com.example.duckduckpizza.R;
import com.example.duckduckpizza.adapter.PizzaAdapter;
import com.example.duckduckpizza.dto.PizzaResponseDTO;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PizzaListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pizza_list);
        ListView listView = findViewById(R.id.pizza_item);
        Call<List<PizzaResponseDTO>> call = NetworkService.getInstance().getJSONApi().getPizzaData();
        Context context = this;
        call.enqueue(new Callback<List<PizzaResponseDTO>>() {
            @Override
            public void onResponse(Call<List<PizzaResponseDTO>> call, Response<List<PizzaResponseDTO>> response) {
                System.out.println(response.body());
                PizzaAdapter pizzaAdapter = new PizzaAdapter(context, response.body());
                listView.setAdapter(pizzaAdapter);
            }

            @Override
            public void onFailure(Call<List<PizzaResponseDTO>> call, Throwable t) {

            }
        });
        listView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(PizzaListActivity.this, PizzaDetailActivity.class);
            intent.putExtra("pizzaId", id);
            startActivity(intent);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_pizza:
                startActivity(new Intent(this, PizzaListActivity.class));
                return true;
            case R.id.action_cart:
                startActivity(new Intent(this, CartActivity.class));
                return true;
            case R.id.action_order:
                startActivity(new Intent(this, OrderListActivity.class));
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }
}

