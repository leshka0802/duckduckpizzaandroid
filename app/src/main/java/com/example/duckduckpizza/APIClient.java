package com.example.duckduckpizza;

import com.example.duckduckpizza.dto.CartItemRequestDTO;
import com.example.duckduckpizza.dto.CartItemResponseDTO;
import com.example.duckduckpizza.dto.OrderResponseDTO;
import com.example.duckduckpizza.dto.PizzaResponseDTO;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIClient {

    @GET("api/pizza/{pizza}/")
    Call<PizzaResponseDTO> getPizzaDetail(@Path("pizza") Long pizzaId);

    @GET("api/pizza/")
    Call<List<PizzaResponseDTO>> getPizzaData();

    @GET("api/cart_item/")
    Call<List<CartItemResponseDTO>> getCartItemData(@Header("Authorization") String auth);

    @POST("api/cart_item/")
    Call<CartItemResponseDTO> createCartItem(@Header("Authorization") String auth, @Body CartItemRequestDTO body);

    @DELETE("api/cart_item/{cartItemId}/")
    Call<Void> deleteCartItem(@Header("Authorization") String auth, @Path("cartItemId") Long cartItemId);

    @GET("api/order/")
    Call<List<OrderResponseDTO>> getOrderData(@Header("Authorization") String auth);

    @POST("/api/order/create/")
    Call<Void> makeOrder(@Header("Authorization") String auth);
}
