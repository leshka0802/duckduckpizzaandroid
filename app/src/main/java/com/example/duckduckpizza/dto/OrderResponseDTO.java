package com.example.duckduckpizza.dto;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class OrderResponseDTO{

	@SerializedName("total_price")
	private String totalPrice;

	@SerializedName("id")
	private int id;

	@SerializedName("orderitem_set")
	private List<OrderitemSetItem> orderitemSet;

	public void setTotalPrice(String totalPrice){
		this.totalPrice = totalPrice;
	}

	public String getTotalPrice(){
		return totalPrice;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setOrderitemSet(List<OrderitemSetItem> orderitemSet){
		this.orderitemSet = orderitemSet;
	}

	public List<OrderitemSetItem> getOrderitemSet(){
		return orderitemSet;
	}

	@Override
 	public String toString(){
		return 
			"OrderResponseDTO{" + 
			"total_price = '" + totalPrice + '\'' + 
			",id = '" + id + '\'' + 
			",orderitem_set = '" + orderitemSet + '\'' + 
			"}";
		}
}