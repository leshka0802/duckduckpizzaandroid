package com.example.duckduckpizza.dto;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class CartItemRequestDTO {

	@SerializedName("amount")
	private int amount;

	@SerializedName("pizza")
	private int pizza;

	@SerializedName("size")
	private int size;


	public void setAmount(int amount){
		this.amount = amount;
	}

	public int getAmount(){
		return amount;
	}

	public void setPizza(int pizza){
		this.pizza = pizza;
	}

	public int getPizza(){
		return pizza;
	}

	public void setSize(int size){
		this.size = size;
	}

	public int getSize(){
		return size;
	}


	@Override
 	public String toString(){
		return 
			"Response{" + 
			"amount = '" + amount + '\'' + 
			",pizza = '" + pizza + '\'' +
			",size = '" + size + '\'' + 
			"}";
		}
}