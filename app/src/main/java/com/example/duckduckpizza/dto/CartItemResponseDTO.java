package com.example.duckduckpizza.dto;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class CartItemResponseDTO {

	@SerializedName("amount")
	private int amount;

	@SerializedName("get_size_display")
	private String getSizeDisplay;

	@SerializedName("pizza")
	private int pizza;

	@SerializedName("size")
	private int size;

	@SerializedName("pizza_detail")
	private PizzaResponseDTO pizzaDetail;

	@SerializedName("id")
	private int id;

	@SerializedName("cart")
	private int cart;

	public void setAmount(int amount){
		this.amount = amount;
	}

	public int getAmount(){
		return amount;
	}

	public void setGetSizeDisplay(String getSizeDisplay){
		this.getSizeDisplay = getSizeDisplay;
	}

	public String getGetSizeDisplay(){
		return getSizeDisplay;
	}

	public void setPizza(int pizza){
		this.pizza = pizza;
	}

	public int getPizza(){
		return pizza;
	}

	public void setSize(int size){
		this.size = size;
	}

	public int getSize(){
		return size;
	}

	public void setPizzaDetail(PizzaResponseDTO pizzaDetail){
		this.pizzaDetail = pizzaDetail;
	}

	public PizzaResponseDTO getPizzaDetail(){
		return pizzaDetail;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setCart(int cart){
		this.cart = cart;
	}

	public int getCart(){
		return cart;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"amount = '" + amount + '\'' + 
			",get_size_display = '" + getSizeDisplay + '\'' + 
			",pizza = '" + pizza + '\'' + 
			",size = '" + size + '\'' + 
			",pizza_detail = '" + pizzaDetail + '\'' + 
			",id = '" + id + '\'' + 
			",cart = '" + cart + '\'' + 
			"}";
		}
}