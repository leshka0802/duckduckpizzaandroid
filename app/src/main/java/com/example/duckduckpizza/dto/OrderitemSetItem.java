package com.example.duckduckpizza.dto;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class OrderitemSetItem{

	@SerializedName("amount")
	private int amount;

	@SerializedName("pizza")
	private int pizza;

	@SerializedName("pizza_detail")
	private PizzaResponseDTO pizzaDetail;

	@SerializedName("price")
	private String price;

	@SerializedName("order")
	private int order;

	public void setAmount(int amount){
		this.amount = amount;
	}

	public int getAmount(){
		return amount;
	}

	public void setPizza(int pizza){
		this.pizza = pizza;
	}

	public int getPizza(){
		return pizza;
	}

	public void setPizzaDetail(PizzaResponseDTO pizzaDetail){
		this.pizzaDetail = pizzaDetail;
	}

	public PizzaResponseDTO getPizzaDetail(){
		return pizzaDetail;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setOrder(int order){
		this.order = order;
	}

	public int getOrder(){
		return order;
	}

	@Override
 	public String toString(){
		return 
			"OrderitemSetItem{" + 
			"amount = '" + amount + '\'' + 
			",pizza = '" + pizza + '\'' + 
			",pizza_detail = '" + pizzaDetail + '\'' + 
			",price = '" + price + '\'' + 
			",order = '" + order + '\'' + 
			"}";
		}
}