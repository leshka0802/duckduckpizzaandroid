package com.example.duckduckpizza.dto;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PizzaResponseDTO {

	@SerializedName("price_small")
	private String priceSmall;

	@SerializedName("name")
	private String name;

	@SerializedName("price_middle")
	private String priceMiddle;

	@SerializedName("available")
	private boolean available;

	@SerializedName("description")
	private String description;

	@SerializedName("id")
	private int id;

	@SerializedName("price_a_piece")
	private String priceAPiece;

	@SerializedName("price_large")
	private String priceLarge;

	public void setPriceSmall(String priceSmall){
		this.priceSmall = priceSmall;
	}

	public String getPriceSmall(){
		return priceSmall;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setPriceMiddle(String priceMiddle){
		this.priceMiddle = priceMiddle;
	}

	public String getPriceMiddle(){
		return priceMiddle;
	}

	public void setAvailable(boolean available){
		this.available = available;
	}

	public boolean isAvailable(){
		return available;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setPriceAPiece(String priceAPiece){
		this.priceAPiece = priceAPiece;
	}

	public String getPriceAPiece(){
		return priceAPiece;
	}

	public void setPriceLarge(String priceLarge){
		this.priceLarge = priceLarge;
	}

	public String getPriceLarge(){
		return priceLarge;
	}

	@Override
 	public String toString(){
		return 
			"PizzaDetail{" + 
			"price_small = '" + priceSmall + '\'' + 
			",name = '" + name + '\'' + 
			",price_middle = '" + priceMiddle + '\'' + 
			",available = '" + available + '\'' + 
			",description = '" + description + '\'' + 
			",id = '" + id + '\'' + 
			",price_a_piece = '" + priceAPiece + '\'' + 
			",price_large = '" + priceLarge + '\'' + 
			"}";
		}
}