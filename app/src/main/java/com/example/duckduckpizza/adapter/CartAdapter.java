package com.example.duckduckpizza.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.duckduckpizza.R;
import com.example.duckduckpizza.dto.CartItemResponseDTO;

import java.util.List;


public class CartAdapter extends BaseAdapter {

    private Context context;
    private List<CartItemResponseDTO> cartItemList;

    public CartAdapter(Context context, List<CartItemResponseDTO> cartItemList) {
        this.context = context;
        this.cartItemList = cartItemList;
    }

    @Override
    public int getCount() {
        return cartItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return cartItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return cartItemList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CartViewHolder holder;

        if (convertView == null) {
            holder = new CartViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cart_item, null, true);
            holder.name = convertView.findViewById(R.id.name);
            holder.amount = convertView.findViewById(R.id.cartAmount);
            holder.size = convertView.findViewById(R.id.cartSize);
            convertView.setTag(holder);
        } else {
            holder = (CartViewHolder) convertView.getTag();
        }

        holder.name.setText(cartItemList.get(position).getPizzaDetail().getName());
        holder.amount.setText(Integer.toString(cartItemList.get(position).getAmount()));
        holder.size.setText(cartItemList.get(position).getGetSizeDisplay());
        return convertView;
    }

    private class CartViewHolder {
        protected TextView name, amount, size;
    }
}