package com.example.duckduckpizza.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.duckduckpizza.R;
import com.example.duckduckpizza.dto.PizzaResponseDTO;

import java.util.List;


public class PizzaAdapter extends BaseAdapter {

    private Context context;
    private List<PizzaResponseDTO> pizzaList;

    public PizzaAdapter(Context context, List<PizzaResponseDTO> pizzaList) {
        this.context = context;
        this.pizzaList = pizzaList;
    }

    @Override
    public int getCount() {
        return pizzaList.size();
    }

    @Override
    public Object getItem(int position) {
        return pizzaList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return pizzaList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PizzaViewHolder holder;

        if (convertView == null) {
            holder = new PizzaViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.pizza_item, null, true);

            holder.name = convertView.findViewById(R.id.name);
            holder.description = convertView.findViewById(R.id.description);
            holder.priceAPiece = convertView.findViewById(R.id.priceAPiece);
            holder.priceSmall = convertView.findViewById(R.id.priceSmall);
            holder.priceMiddle = convertView.findViewById(R.id.priceMiddle);
            holder.priceLarge = convertView.findViewById(R.id.priceLarge);
            convertView.setTag(holder);
        }else {
            holder = (PizzaViewHolder) convertView.getTag();
        }

        holder.name.setText(pizzaList.get(position).getName());
        holder.description.setText(pizzaList.get(position).getDescription());
        holder.priceAPiece.setText(pizzaList.get(position).getPriceAPiece());
        holder.priceSmall.setText(pizzaList.get(position).getPriceSmall());
        holder.priceMiddle.setText(pizzaList.get(position).getPriceMiddle());
        holder.priceLarge.setText(pizzaList.get(position).getPriceLarge());

        return convertView;
    }

    private class PizzaViewHolder {
        protected TextView name, description, priceAPiece, priceSmall, priceMiddle, priceLarge;
    }
}