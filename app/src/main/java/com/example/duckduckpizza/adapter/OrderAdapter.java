package com.example.duckduckpizza.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.duckduckpizza.R;
import com.example.duckduckpizza.dto.OrderResponseDTO;
import com.example.duckduckpizza.dto.OrderResponseDTO;

import java.util.List;


public class OrderAdapter extends BaseAdapter {

    private Context context;
    private List<OrderResponseDTO> orderItemList;

    public OrderAdapter(Context context, List<OrderResponseDTO> orderItemList) {
        this.context = context;
        this.orderItemList = orderItemList;
    }

    @Override
    public int getCount() {
        return orderItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return orderItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return orderItemList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OrderViewHolder holder;

        if (convertView == null) {
            holder = new OrderViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.order_item, null, true);
            holder.orderId = convertView.findViewById(R.id.orderId);
            holder.totalPrice = convertView.findViewById(R.id.totalPrice);
            holder.orderDetail = convertView.findViewById(R.id.order_detail_item);
            convertView.setTag(holder);
        } else {
            holder = (OrderViewHolder) convertView.getTag();
        }

        holder.orderId.setText(Integer.toString(orderItemList.get(position).getId()));
        holder.totalPrice.setText(orderItemList.get(position).getTotalPrice());
        OrderDetailAdapter orderDetailAdapter = new OrderDetailAdapter(context, orderItemList.get(position).getOrderitemSet());
        holder.orderDetail.setAdapter(orderDetailAdapter);
        return convertView;
    }

    private class OrderViewHolder {
        protected TextView orderId, totalPrice;
        protected ListView orderDetail;
    }
}