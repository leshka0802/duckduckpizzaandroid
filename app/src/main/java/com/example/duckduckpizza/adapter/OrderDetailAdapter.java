package com.example.duckduckpizza.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.duckduckpizza.R;
import com.example.duckduckpizza.dto.OrderitemSetItem;

import java.util.List;


public class OrderDetailAdapter extends BaseAdapter {

    private Context context;
    private List<OrderitemSetItem> orderItemList;

    public OrderDetailAdapter(Context context, List<OrderitemSetItem> orderItemList) {
        this.context = context;
        this.orderItemList = orderItemList;
    }

    @Override
    public int getCount() {
        return orderItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return orderItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OrderDetailViewHolder holder;

        if (convertView == null) {
            holder = new OrderDetailViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.order_detail_item, null, true);
            holder.pizzaName = convertView.findViewById(R.id.orderDetailPizzaName);
            holder.amount = convertView.findViewById(R.id.orderDetailAmount);
            holder.price = convertView.findViewById(R.id.orderDetailPrice);
            convertView.setTag(holder);
        } else {
            holder = (OrderDetailViewHolder) convertView.getTag();
        }

        holder.pizzaName.setText(orderItemList.get(position).getPizzaDetail().getName());
        holder.amount.setText(Integer.toString(orderItemList.get(position).getAmount()));
        holder.price.setText(orderItemList.get(position).getPrice());
        return convertView;
    }

    private class OrderDetailViewHolder {
        protected TextView pizzaName, amount, price;
    }
}